// 	Простой HTTP REST API сервис
// 	база данных содержит записи типа
// 	type User struct {
//		ID   int    `json:"id"`
//		Name string `json:"name"`
//	}
// http://localhost:8000/users все записи базы данных
// http://localhost:8000/users/{id} запись с заданным id,
//		или {ID: 0, Name: ""} если запись с таким id не найдена
// http://localhost:8000/users/add/{name} добавляет запись с именем name.
//		Возвращает запись в формате User
// http://localhost:8000/users/add/{name} добавляет запись с именем name.
// http://localhost:8000/users/del/{id} удаляет запись с заданным id.
//		Возвращает {ID: id, Name: "deleted"} или {ID: 0, Name: ""},
//		если запись с таким id не найдена
// http://localhost:8000/users/upd/{id}/{name} заменяет name в записи с заданным id
//		 Возвращает {ID: id, Name: name} или {ID: 0, Name: ""},
//		если запись с таким id не найдена

package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"github.com/gorilla/mux"
	"net/http"

)

var db *sql.DB

func main() {
	var err error
	db, err = sql.Open("sqlite3", "./mybase.db")
	fatal(err)
	defer db.Close()
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS " +
		`names("id" INTEGER PRIMARY KEY AUTOINCREMENT,` +
		`"name" VARCHAR(50))`)
	fatal(err)
    router := mux.NewRouter()
    router.HandleFunc("/users", getUsers).Methods("GET")
    router.HandleFunc("/users/{id}", getUser).Methods("GET")
    router.HandleFunc("/users/add/{name}", addUser).Methods("GET")
	router.HandleFunc("/users/del/{id}", delUser).Methods("GET")
	router.HandleFunc("/users/upd/{id}/{name}", updUser).Methods("GET")
	

	err = http.ListenAndServe(":8000", router)
	fatal(err)
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
