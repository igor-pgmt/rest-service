package main

import "fmt"

func selectAll() []*User {
	rows, err := db.Query("SELECT * FROM names")
	fatal(err)
	defer rows.Close()
	users := make([]*User, 0)
	for rows.Next() {
		user := new(User)
		err := rows.Scan(&user.ID, &user.Name)
		fatal(err)
		users = append(users, user)
	}
	fmt.Println(users)
	return users
}

func selectUser(id int) User {
	row := db.QueryRow("SELECT * FROM names WHERE id = $1", id)
	var user User
	row.Scan(&user.ID, &user.Name)
	return user
}

func insertUser(name string) User {
	stmt, err := db.Prepare("INSERT INTO names(name) values(?)")
	fatal(err)
	res, err := stmt.Exec(name)
	fatal(err)
	lastID, err := res.LastInsertId()
	fatal(err)
	return User{ID: int(lastID), Name: name}
}

func deleteUser(id int) bool {
	stmt, err := db.Prepare("DELETE FROM names WHERE id=?")
	fatal(err)
	res, err := stmt.Exec(id)
	fatal(err)
	affect, err := res.RowsAffected()
	fatal(err)
	return affect == 1
}

func updateUser(user User) bool {
	stmt, err := db.Prepare("UPDATE names SET name=? WHERE id=?")
	fatal(err)
	res, err := stmt.Exec(user.Name, user.ID)
	fatal(err)
	affect, err := res.RowsAffected()
	fatal(err)
	return affect == 1
}
